import React from 'react';
import './App.css';
import Routes from './Routes';
import Header from './layout/Header';
import Footer from './layout/Footer';


function App() {
    return (
    <div className="App">
        <Header/>
        <Routes/>
        <Footer/>
    </div>
    );
}

export default App;
