import React, { Component } from 'react';
import LoginForm from '../components/LoginForm';


export class Login extends Component {
    constructor (){
        super();
        this.state = {
            title:"Connectez-vous",
            subtitle:"Entrer votre mot de passe et identifiant",
            myProducts: [
                {
                    id: 1,
                    name: "Air Force one",
                    price: 18
                },
                {
                    id: 2,
                    name: "Running",
                    price: 18
                },
                {
                    id: 3,
                    name: "Cortez",
                    price: 18
                }
            ]
        }
    }
    render() {
        return (
            <div>
                <h1>
                    {this.state.title}
                </h1>
                <h2>
                    {this.state.subtitle}
                </h2>
                <div className="productList">
                    {this.state.myProducts.map(product =>(
                        <div>
                            { product.id }
                            { product.name }
                            { product.price }
                        </div>
                    ))}
                </div>
                <LoginForm/>
            </div>
        );
    }
}
export default Login;