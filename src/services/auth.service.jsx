import jwtDecode from 'jwt-decode';

export default class AuthService {
    login(body){
        return fetch('http://localhost:3000/api/v1/auth/login/',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
                body: JSON.stringify(body)
        }).then(function(res){
            
            return res.json;
        })
        
    }
    getToken(){
        return localStorage.getItem('token');
    }
    getUserProfil(){
        return jwtDecode(this.getToken())
    }
    getuserdetail(id) {
        return fetch(`http://localhost:3000/api/v1/users/${id}`)
            .then( function(res){
                return res.json();
            })
    }
}