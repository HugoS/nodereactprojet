import React, { Component } from 'react';
import {Route} from 'react-router-dom';
import Login from './pages/Login';
import Home from './pages/Home';
import Account from './pages/Account';

export class Routes extends Component {
    render() {
        return (
            <div>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/" component={Home}/>
                <Route exact path="/account" component={Account}/>
            </div>
        )
    }
}

export default Routes;