import React, { Component } from 'react';
import {Link,} from 'react-router-dom';

export  class Header extends Component {
    render() {
        return (
            <header>
                <Link to="/login">Login</Link>
                <Link to="/">Home</Link>
                <Link to="/account">Mon compte</Link>
            </header>
        );
    }
}
export default Header;